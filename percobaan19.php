<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>percobaan19:Contoh Layout 1 Kolom</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <style>
        body {
            background-color: #d4f4d7;
        }

        header {
            background-color: #41b983;
        }

        footer {
            background-color: #41b983;
        }
    </style>
</head>

<body>

    <header class="container bg-primary text-white">
        <div class="row">
            <div class="col-12 py-4 text-center">
                <h1 class="display-1">Tutorial Bootstrap</h1>
                <p class="lead">Belajar Bootstrap untuk Pemula</p>
            </div>
        </div>
    </header>

    <main class="container border">
        <div class="row">
            <div class="col-md-8 py-5">
                <h1>Layout Satu Kolom</h1>
                <p>Isi</p>
            </div>
            <div class="col-md-4 py-5">
                <img class="w-100" src="https://loremflickr.com/150/150/dog" alt="gambar anjing">
            </div>
        </div>
    </main>

    <footer class="container bg-primary text-white">
        <div class="row">
            <div class="col-12 py-4">
                &copy; 2023 @WEND Tutorial Bootstrap
            </div>
        </div>
    </footer>

</body>

</html>
