<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>percobaan10: JavaScript - if/else/if</title>
	<style>
		body {
			background-color: #f2f2f2;
			font-family: Arial, sans-serif;
			text-align: center;
		}

		h2 {
			color: #333;
		}

		.grade {
			font-size: 24px;
			font-weight: bold;
			color: #fff;
			padding: 10px;
			border-radius: 5px;
			display: inline-block;
		}

		.grade-a {
			background-color: #4caf50;
		}

		.grade-bplus {
			background-color: #2196f3;
		}

		.grade-b {
			background-color: #03a9f4;
		}

		.grade-cplus {
			background-color: #ff9800;
		}

		.grade-c {
			background-color: #ffc107;
		}

		.grade-d {
			background-color: #ff5722;
		}

		.grade-e {
			background-color: #f44336;
		}

		.grade-f {
			background-color: #9e9e9e;
		}
	</style>
</head>
<body>
	<script type="text/javascript">
		var nilai = prompt("Inputkan nilai akhir");
		var grade = "";

		if (nilai >= 90) {
			grade = "A";
			document.write(`<h2>Selamat! Nilai Anda sangat memuaskan!</h2>`);
		} else if (nilai >= 80) {
			grade = "B+";
			document.write(`<h2>Bagus! Nilai Anda cukup baik!</h2>`);
		} else if (nilai >= 70) {
			grade = "B";
			document.write(`<h2>Nilai Anda cukup bagus!</h2>`);
		} else if (nilai >= 60) {
			grade = "C+";
			document.write(`<h2>Anda lulus dengan nilai cukup!</h2>`);
		} else if (nilai >= 50) {
			grade = "C";
			document.write(`<h2>Anda lulus dengan nilai cukup!</h2>`);
		} else if (nilai >= 40) {
			grade = "D";
			document.write(`<h2>Anda lulus dengan nilai cukup!</h2>`);
		} else if (nilai >= 30) {
			grade = "E";
			document.write(`<h2>Silakan ikuti remedial!</h2>`);
		} else {
			grade = "F";
			document.write(`<h2>Selamatkan!</h2>`);
		}

		document.write(`<p>Grade Anda adalah: <span class="grade grade-${grade.toLowerCase()}">${grade}</span></p>`);
	</script>
</body>
</html>
