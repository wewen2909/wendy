<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>percobaan13 Javascript</title>
</head>
<body>
	<button onclick="displayProducts()">Tampilkan Produk</button>
	<ul id="productList"></ul>

	<input type="text" id="inputNilai1" placeholder="Nilai 1">
	<input type="text" id="inputNilai2" placeholder="Nilai 2">
	<button onclick="hitungPembagian()">Hitung Pembagian</button>
	<p id="result"></p>

	<script type="text/javascript">
		function displayProducts() {
			var products = ["Senter", "Radio", "Antena", "Obeng"];
			var productList = document.getElementById("productList");
			productList.innerHTML = ""; // Mengosongkan daftar sebelum menambahkan produk baru

			for (let i = 0; i < products.length; i++) {
				var listItem = document.createElement("li");
				listItem.textContent = products[i];
				productList.appendChild(listItem);
			}
		}

		function hitungPembagian() {
			var nilai1 = parseFloat(document.getElementById("inputNilai1").value);
			var nilai2 = parseFloat(document.getElementById("inputNilai2").value);

			if (isNaN(nilai1) || isNaN(nilai2)) {
				document.getElementById("result").textContent = "Masukkan angka pada kedua input";
				return;
			}

			var hasilPembagian = nilai1 / nilai2 + 30;
			document.getElementById("result").textContent = "Hasil Pembagian: " + hasilPembagian;
		}
	</script>
</body>
</html>
