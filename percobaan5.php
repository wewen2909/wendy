<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>percobaan 5: Javascript</title>
  <style>
    body {
      font-family: Arial, sans-serif;
      text-align: center;
      padding-top: 100px;
    }

    h1 {
      color: #333;
      font-size: 32px;
    }

    .button {
      display: inline-block;
      padding: 10px 20px;
      background-color: #26d548;
      color: #fff;
      text-decoration: none;
      font-size: 16px;
      border-radius: 4px;
      transition: background-color 0.3s;
    }

    .button:hover {
      background-color: #26d548;
    }
  </style>
</head>
<body>
  <h1>Latihan 5: JavaScript</h1>
  <p id="demo"></p>
  <a href="#" onclick="showAlert()" class="button" style="font-weight: bold;">Klik Saya</a>


  <script>
    console.log("Hello JS dari header");

    function showAlert() {
      alert("Yey!");
    }

    document.addEventListener("DOMContentLoaded", function() {
      console.log("Saya belajar JavaScript");
      document.getElementById("demo").innerText = "Hello World";
    });
  </script>
</body>
</html>
