<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Latihan 1 : Internal CSS</title>
	<style type="text/css">
		body{
			background-color: #7e7e7e;
		}

		/*h1{
			color : white;
			text-align: center;
		}*/

		p{
			font-family: verdana;
			font-size: 20px;
		}

		.myClass{
			font: bold 1.25em times;
			color: #00ff00;
		}
    
	</style>
</head>
<body style="color: red; font-weight: bold;">
	Saya Mencoba CSS

	<h1 style="color: #00ff00;">Tag Heading h1: Fungsi tag ini digunakan pada nama blog </h1>

	<p style="color: #00ff00;"> Tag p(paragraf) merupakan elemen HTML yang digunakan untuk menandai sekumpulan teks sebagai suatu paragraf.</p>

	<h1 class="myClass">Ini adalah H1 myclass h1 digunakan untuk membuat sebuah judul s</h1>
	<p class="myClass">Ini adalah P myclass p digunakan untuk membuat sebuah paragraf (p) dengan kelas "myClass"</p>
</body>
</html>
