<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Percobaan9: Javascript IF/ELSE</title>
	<style>
		h2 {
			color: #333;
			text-align: center;
		}

		.success {
			color: green;
		}

		.error {
			color: red;
		}

		p {
			text-align: center;
		}
	</style>
</head>
<body>
	<script type="text/javascript">
		var password = prompt("Password:");

		if (password === "kopi") {
			document.write("<h2 class='success'>Selamat datang di website kami</h2>");
		} else {
			document.write("<h2 class='error'>Password salah, coba lagi</h2>");
		}

		document.write("<p>Terima kasih sudah menggunakan aplikasi ini</p>");
	</script>
</body>
</html>
