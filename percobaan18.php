<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>percobaan18: Contoh Layout 1 Kolom</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <style>
        .header {
            background-color: #008000;
            color: #ffffff;
        }

        .main {
            border: 1px solid #008000;
            background-color: #ffffff;
            padding: 20px;
        }

        .footer {
            background-color: 	#8FBC8F;
        }
    </style>
</head>

<body>
    <header class="container header">
        <div class="row">
            <div class="col-12 py-4 text-center">
                <h1 class="display-1">Bootstrap</h1>
                <p class="lead">Belajar Bootstrap</p>
            </div>
        </div>
    </header>

    <main class="container main">
        <div class="row">
            <div class="col-12 py-5">
                <h1>Layout Satu Kolom</h1>
                <p>Ini isinya okey</p>
            </div>
        </div>
    </main>

    <footer class="container footer">
        <div class="row">
            <div class="col-12 py-4">
                &copy; 2023 @WENDY Tutorial Bootstrap
            </div>
        </div>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
        crossorigin="anonymous"></script>
</body>

</html>
