<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>percobaan16: Contoh Container</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
  <style>
    body {
      background-color: #f8f9fa;
    }

    header {
      padding: 20px;
      background-color: #6c757d;
    }

    h1 {
      color: #fff;
      font-size: 36px;
      text-align: center;
      text-transform: uppercase;
      letter-spacing: 2px;
    }
  </style>
</head>

<body>

  <header>
    <div class="container bg-info text-white">
      <h1>Supaya lebih menarik aja </h1>
    </div>
  </header>

  <div class="container mt-5">
    <div class="row">
      <div class="col-lg-6">
        <h2>Heading 2</h2>
        <p>WENDY DI SINI HEADING 2</p>
      </div>
      <div class="col-lg-6">
        <h2>Heading 2</h2>
        <p>AMAN DI SINI ADA Heading 2 JUGA </p>
      </div>
    </div>
  </div>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
    crossorigin="anonymous"></script>
</body>

</html>
