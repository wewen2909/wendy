<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>percobaan17:Contoh Gutter</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <style>
        .custom-frame {
            border: 5px solid #f0f0f0;
            border-radius: 10px;
            padding: 10px;
        }
    </style>
</head>

<body>

    <div class="container">
        <div class="row g-5">
            <div class="col-md-3">
                <div class="custom-frame">
                    <img class="w-100" src="https://loremflickr.com/150/150/dog" alt="gambar anjing">
                </div>
            </div>
            <div class="col-md-3">
                <div class="custom-frame">
                    <img class="w-100" src="https://loremflickr.com/150/150/dog" alt="gambar anjing">
                </div>
            </div>
            <div class="col-md-3">
                <div class="custom-frame">
                    <img class="w-100" src="https://loremflickr.com/150/150/dog" alt="gambar anjing">
                </div>
            </div>
            <div class="col-md-3">
                <div class="custom-frame">
                    <img class="w-100" src="https://loremflickr.com/150/150/dog" alt="gambar anjing">
                </div>
            </div>
            <div class="col-md-3">
                <div class="custom-frame">
                    <img class="w-100" src="https://loremflickr.com/150/150/dog" alt="gambar anjing">
                </div>
            </div>
            <div class="col-md-3">
                <div class="custom-frame">
                    <img class="w-100" src="https://loremflickr.com/150/150/dog" alt="gambar anjing">
                </div>
            </div>
            <div class="col-md-3">
                <div class="custom-frame">
                    <img class="w-100" src="https://loremflickr.com/150/150/dog" alt="gambar anjing">
                </div>
            </div>
            <div class="col-md-3">
                <div class="custom-frame">
                    <img class="w-100" src="https://loremflickr.com/150/150/dog" alt="gambar anjing">
                </div>
            </div>
        </div>
    </div>

</body>

</html>
