<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>percobaan 8: Javascript Kondisi IF</title>
</head>
<body>
	<script type="text/javascript">
		var totalBelanja = prompt("Masukkan total belanja Anda:", 0);
		totalBelanja = parseFloat(totalBelanja);

		if (isNaN(totalBelanja)) {
			alert("Masukkan angka yang valid.");
		} else {
			document.write("<h2>Terima kasih telah berbelanja di toko kami!</h2>");

			if (totalBelanja > 100000) {
				document.write("<p>Selamat! Anda berhak mendapatkan hadiah dari kami.</p>");
			} else {
				document.write("<p>Maaf, Anda belum memenuhi syarat untuk mendapatkan hadiah.</p>");
			}
		}
	</script>
</body>
</html>
