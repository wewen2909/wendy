<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Percobaan 7 JavaScript: Variable</title>
	<script type="text/javascript">
		// Membuat variabel
		var name = "WENDY AMAN SIANTURI";
		var visitorCount = 2909;
		var isActive = true;
		var url = "https://polines.ac.id";

		// Menampilkan variabel di jendela dialog (alert)
		alert("Selamat datang di website: " + name);

		// Menampilkan variabel ke dalam HTML
		document.write("Nama website kampus saya: " + name + "<br>");
		document.write("Jumlah Pengunjung setiap harinya: " + visitorCount + "<br>");
		document.write("Status aktif: " + isActive + "<br>");
		document.write("Alamat URL: " + url + "<br>");
	</script>
</head>
<body>
	<?php
	$nama = "Wendy ";
	$nama2 = "Aman ";
	$nama3 = "Sianturi ";
	$nama_gabung = $nama . $nama2 . $nama3;
	echo $nama_gabung;
	?>
</body>
</html>
